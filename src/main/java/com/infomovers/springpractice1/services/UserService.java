package com.infomovers.springpractice1.services;

import com.infomovers.springpractice1.dao.UserDAO;
import com.infomovers.springpractice1.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserDAO dao;

    public List<User> getUsers() {
        return dao.getUsers();
    }

    public List<User> getUsersByAddressAndDate(String address, LocalDate date) {
        return dao.getUsersByAddressAndDate(address, date);
    }

    public List<User> getUsersByFirstNameOrLastName(String firstname, String lastname) {

        return dao.getUsersByFirstNameOrLastName(firstname, lastname);
    }

    public List<User> getUsersByFirstName(String name) {

        return dao.getUserByFirstName(name);
    }

    public List<User> getUsersByDateBetween(LocalDate date1, LocalDate date2) {

        return dao.getUserByDateBetween(date1, date2);
    }

    public List<User> getUsersBetweenIdNumbers(int id1, int id2) {

        return dao.getUserBetweenIdNumbers(id1, id2);
    }

    public List<User> getUsersAgeLessThan(int age) {
        return dao.getUsersAgeLessThan(age);
    }

    public List<User> getUsersAgeLessThanEqual(int age) {
        return dao.getUsersAgeLessThanEqual(age);
    }

    public List<User> getUsersAgeGreaterThan(int age) {
        return dao.getUsersAgeGreaterThan(age);
    }

    public List<User> getUsersAgeGreaterThanEqual(int age) {
        return dao.getUsersAgeGreaterThanEqual(age);
    }

    public List<User> getUsersAfterDate(LocalDate date) {
        return dao.getUsersAfterDate(date);
    }

    public List<User> getUsersBeforeDate(LocalDate date) {
        return dao.getUsersBeforeDate(date);
    }

    public List<User> getUsersAfterId(int id) {
        return dao.getUsersIdAfter(id);
    }

    public List<User> getUsersAgeNull() {
        return dao.getUserAgeNull();
    }

    public List<User> getUsersAgeNotNull() {
        return dao.getUserAgeNotNull();
    }

    public List<User> getUsersFirstNameNull() {
        return dao.getUserFirstNameNull();
    }

    public List<User> getUsersFirstNameLike(String name) {
        return dao.getUsersFirstNameLike(name);
    }

    public List<User> getUsersFirstNameNotLike(String name) {
        return dao.getUsersFirstNAmeNotLike(name);
    }

    public List<User> getUsersByNameStaringWith(String name) {
        return dao.getUsersByNameStartingWith(name);
    }

    public List<User> getUsersByNameEndingWith(String name) {
        return dao.getUsersByNameEndingWith(name);
    }

    public List<User> getUsersByNameContaining(String name) {
        return dao.getUsersByNameContaining(name);
    }

    public List<User> getUsersByAgeOrderName(String name) {
        return dao.getUsersByAgeOrderByName(name);
    }

    public List<User> getUsersByLastNameOrder(String name) {
        return dao.getUsersByLastnameOrderByFirstname(name);
    }

    public List<User> getUsersByLastNameNot(String name) {
        return dao.getUsersByLastNameNot(name);
    }

    public List<User> getUsersByAgeIn(Collection<Integer> ages) {
        return dao.getUsersByAgeIn(ages);
    }

    public List<User> getUsersByNameNotIn(Collection<String> name) {
        return dao.getUsersByFirstNameNotIn(name);
    }

    public List<User> getUsersByMarried() {
        return dao.getUserByMarriedTrue();
    }

    public List<User> getUserByNotMarriedAndLastName(String name) {
        return dao.getUserByNotMarriedAndLastName(name);
    }


    public User getUserById(int id) {
        return dao.getUserById(id);
    }

    public void CreateUser(User user) {
        dao.CreateUser(user);
    }

    public void DeleteUser(int id) {
        dao.DeleteUserById(id);
    }

    public User UpdateUser(int id, User user) {
        return dao.UpdateUserById(id, user);

    }

    public String ReturnUserNameById(int id) {
        Optional<User> optionalCustomer = dao.ReturnNameById(id);
        String firstname =optionalCustomer.get().getFirstName();
        String lastname =optionalCustomer.get().getLastNAme();
        String fullName=firstname+"\t"+lastname;
        return fullName;
    }

    public List<User> getUserByNativeQuery(String name){
        return dao.getUsersFirstNameLike(name);
    }
}