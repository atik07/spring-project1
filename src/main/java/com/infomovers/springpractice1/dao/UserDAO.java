package com.infomovers.springpractice1.dao;

import com.infomovers.springpractice1.model.User;
import com.infomovers.springpractice1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Component
public class UserDAO {

    @Autowired
    UserRepository repository;

    @Autowired
    EntityManager entityManager;

    @Transactional
    public List<User> getUsers(){

       return repository.findAll();
    }

    @Transactional
    public List<User>getUsersByAddressAndDate(String address, LocalDate date){

        return  repository.findByAddressAndDate(address, date);
    }

    @Transactional
    public List<User>getUsersByFirstNameOrLastName(String firstName,String lastName){

        return repository.findByFirstnameOrLastname(firstName, lastName);
    }

    @Transactional
    public List<User>getUserByFirstName(String name){

        return repository.findByFirstnameEquals(name);
    }

    @Transactional
    public List<User>getUserByDateBetween(LocalDate date1,LocalDate date2){

        return repository.findByDateBetween(date1,date2);
    }

    @Transactional
    public List<User>getUserBetweenIdNumbers(int id1,int id2){
        return repository.findByIdBetween(id1, id2);
    }

    @Transactional
    public List<User>getUsersAgeLessThan(int age){
        return repository.findByAgeLessThan(age);
    }

    @Transactional
    public List<User>getUsersAgeLessThanEqual(int age){
        return repository.findByAgeLessThanEqual(age);
    }

    @Transactional
    public List<User>getUsersAgeGreaterThan(int age){
        return repository.findByAgeGreaterThan(age);
    }

    @Transactional
    public List<User>getUsersAgeGreaterThanEqual(int age){
        return repository.findByAgeGreaterThanEqual(age);
    }

    @Transactional
    public List<User>getUsersAfterDate(LocalDate date){
        return repository.findByDateAfter(date);
    }

    @Transactional
    public List<User>getUsersBeforeDate(LocalDate date){
        return repository.findByDateBefore(date);
    }

    @Transactional
    public List<User>getUsersIdAfter(int id){
        return repository.findByIdAfter(id);
    }

    @Transactional
    public List<User>getUserAgeNull(){
        return repository.findByAgeIsNull();
    }

    @Transactional
    public List<User>getUserAgeNotNull(){
        return repository.findByAgeNotNull();
    }

    @Transactional
    public List<User>getUserFirstNameNull(){
        return repository.findByFirstnameIsNull();
    }

    @Transactional
    public List<User>getUsersFirstNameLike(String name){
        return repository.findByFirstnameLike(name);
    }

    @Transactional
    public List<User> getUsersFirstNAmeNotLike(String name){
        return repository.findByFirstnameNotLike(name);
    }

    @Transactional
    public List<User> getUsersByNameStartingWith(String name){
        return repository.findByFirstnameStartingWith(name);
    }

    @Transactional
    public List<User> getUsersByNameEndingWith(String name){
        return repository.findByLastnameEndingWith(name);
    }

    @Transactional
    public List<User> getUsersByNameContaining (String name){
        return repository.findByFirstnameContaining(name);
    }

    @Transactional
    public  List<User> getUsersByAgeOrderByName(String name) {
        return repository.findByFirstnameOrderByFirstname(name);
    }

    @Transactional
    public List<User> getUsersByLastnameOrderByFirstname(String name){
        return repository.findByLastnameOrderByFirstnameDesc(name);
    }

    @Transactional
    public List<User> getUsersByLastNameNot(String name){
        return repository.findByLastnameNot(name);
    }

    @Transactional
    public List<User> getUsersByAgeIn(Collection<Integer>ages){
        return repository.findByAgeIn(ages);
    }

    @Transactional
    public List<User> getUsersByFirstNameNotIn(Collection<String>name){
        return repository.findByFirstnameNotIn(name);
    }

    @Transactional
    public List<User> getUserByMarriedTrue(){
        return repository.findByMarriedTrue();
    }

    @Transactional
    public List<User> getUserByNotMarriedAndLastName(String name){
        return repository.findByMarriedFalseAndLastnameIs(name);
    }

    @Transactional
    public User getUserById(int id){
        Optional<User> OptUser=repository.findById(id);

        if(OptUser.isPresent()){
            User user=OptUser.get();
            return user;
        }else{
            return null;
        }
    }

    @Transactional
    public void CreateUser(User newUser){
        User user=repository.save(newUser);

    }

    @Transactional
    public void DeleteUserById(int id){
        repository.deleteById(id);
//       User user= repository.findById(id).get();
//       repository.delete(user);

    }

    @Transactional
    public User UpdateUserById(int id,User user){
      Optional<User> optionalUser=repository.findById(id);
      if(optionalUser.isPresent()){
          User existingUser= optionalUser.get();
          repository.save(user);
      }
      return optionalUser.get();

    }

    @Transactional
    public Optional<User> ReturnNameById(int id){
        Optional<User> optionalUser=repository.findById(id);
        return optionalUser;
    }

    @Transactional
    public List<Object[]> getCustomerByQuery(){
        String sql ="Select id,firstname,age from User";
        Query q =entityManager.createNativeQuery(sql);
        List<Object[]> result =q.getResultList();
        return result;

    }

    @Transactional
    public List<User> getCustomerNativeQuery(String name){
       return repository.getUserByNameLikeNativeQuery(name);
    }






}

