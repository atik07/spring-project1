package com.infomovers.springpractice1.restcontroller;

import com.infomovers.springpractice1.dao.UserDAO;
import com.infomovers.springpractice1.model.User;
import com.infomovers.springpractice1.repository.UserRepository;
import com.infomovers.springpractice1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;


@RestController
public class UserRestController {

    @Autowired
    UserService service;

    @Autowired
    UserDAO daoTemp;

    @GetMapping("/users")
    public ResponseEntity<List<User>> handleGetUsers(){

      List<User> users = service.getUsers();

      return new ResponseEntity<>(users, HttpStatus.OK);
    }

//    Using " And  " KeyWord

    /**
     * Using GetMapping to get list of users by  multiple path variable
     * @param address user address
     * @param date user birthdate
     * @return Users list
     */
    @GetMapping( value = "/users/{address}/{date}")
    public ResponseEntity<List<User>> handleGetUsersByAddressAndDate(@PathVariable String address,
                                                                     @PathVariable("date") @DateTimeFormat(pattern="yyyy-MM-dd")LocalDate date)
    {
        List<User> users = service.getUsersByAddressAndDate(address, date);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using Get mapping with multiple parameters
     * @param address User address
     * @param date User Birthdate
     * @return List of Users
     */
    @GetMapping( value = "/users-by-address-date")
    public ResponseEntity<List<User>> handleGetUsersByAddressAndDateUsingParameter(@RequestParam("address") String address,
                                                                     @RequestParam("date") @DateTimeFormat(pattern="yyyy-MM-dd")LocalDate date)
    {
        List<User> users = service.getUsersByAddressAndDate(address, date);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using "Or " keyword to find Users
     * @param firstname Users First name
     * @param lastname Users Last name
     * @return List of users
     */
    @GetMapping(value = "/users-by-name")
    public ResponseEntity<List<User>>handleGetUsersByFirstNAmeOrLastName(@RequestParam("firstname") String firstname,@RequestParam("lastname") String lastname){
        List<User> users =service.getUsersByFirstNameOrLastName(firstname, lastname);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using Keyword "is" ,"equals",simple field to get Users
     * @param firstName Users First name
     * @return List of Users
     */
    @GetMapping("users-firstname")
    public ResponseEntity<List<User>> handleGetUsersByFirstName(@RequestParam("firstname")String firstName){

        List<User> users =service.getUsersByFirstName(firstName);

        return new ResponseEntity<>(users,HttpStatus.OK);

    }

    /**
     * Using Keyword "Between" to find  Users
     * @param date1 Starting date
     * @param date2 Ending date
     * @return List of users Birthday in Between
     */
    @GetMapping("users-date")
    public ResponseEntity<List<User>> handleGetUsersByDateBetween(@RequestParam("date1")@DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date1,
                                                                  @RequestParam("date2")@DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date2){

        List<User>users =service.getUsersByDateBetween(date1, date2);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using Keyword "Between" to find Users
     * @param id1 Starting id
     * @param id2 Ending id
     * @return List of Users
     */
    @GetMapping("users-list")
    public ResponseEntity<List<User>> handleGetUsersBetweenIdNumbers(@RequestParam("id1") int id1,@RequestParam("id2")int id2){

        List<User>users =service.getUsersBetweenIdNumbers(id1, id2);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using "Less Than" Keyword
     * @param age User age
     * @return List of Users
     */
    @GetMapping("/users-age-lessthan")
    public ResponseEntity<List<User>> handleGetUsersAgeLessThan(@RequestParam("agelessthan") int age){

        List<User> users=service.getUsersAgeLessThan(age);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Using "LessThanEqual" Keyword
     * @param age User age
     * @return List of Users
     */

    @GetMapping("/users-age-lessthan-equal")
    public ResponseEntity<List<User>> handleGetUsersAgeLessThanEqual(@RequestParam("agelessthanequal") int age){

        List<User> users=service.getUsersAgeLessThanEqual(age);

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/users-age-greaterthan")
    public ResponseEntity<List<User>> handleUsersAgeGreaterThan(@RequestParam("age") int age){

        List<User>  users=service.getUsersAgeGreaterThan(age);
        return new ResponseEntity<>(users,HttpStatus.OK);

    }

    @GetMapping("/users-age-greaterthan-equal")
    public ResponseEntity<List<User>> handleUsersAgeGreaterThanEqual(@RequestParam("age") int age){

        List<User>  users=service.getUsersAgeGreaterThanEqual(age);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/users-date-after")
    public ResponseEntity<List<User>> handleUsersDateAfter(@RequestParam("date") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date){

        List<User> users=service.getUsersAfterDate(date);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/users-date-before")
    public ResponseEntity<List<User>> handleUsersDateBefore(@RequestParam("date") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date){

        List<User> users = service.getUsersBeforeDate(date);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @GetMapping("/users-after-id")
    public ResponseEntity<List<User>> handleUsersAfterId(@RequestParam("id") int id){

        List<User> users= service.getUsersAfterId(id);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Problem executing IsNull keyword
     * @return Error
     */
    @PostMapping("/users-age-null")
    public ResponseEntity<List<User>> handleUsersAgeNull(){
        List<User> users =service.getUsersAgeNull();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }


    @PostMapping("/users-age-not-null")
    public ResponseEntity<List<User>> handleUserAgeNotNull(){
        List<User> users = service.getUsersAgeNotNull();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    /**
     * Find User where Age is Null
     * @return Users with firstname null
     */
    @PostMapping("/users-firstname-null")
    public ResponseEntity<List<User>> handleUserFirstNameNull(){
        List<User> users=service.getUsersFirstNameNull();

        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-firstname-like")
    public ResponseEntity<List<User>> handleUsersFirstNameLike(@RequestParam("name") String name){

        List<User> users =service.getUsersFirstNameLike(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-firstname-not-like")
    public ResponseEntity<List<User>> handleUsersFirstNameNotLike(@RequestParam("name") String name){

        List<User> users =service.getUsersFirstNameNotLike(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-name-start")
    public ResponseEntity<List<User>> handleUsersByNameStartingWith(@RequestParam String name){

        List<User> users =service.getUsersByNameStaringWith(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-name-end")
    public ResponseEntity<List<User>> handleUsersNameEndingWith(@RequestParam String name){

        List<User> users =service.getUsersByNameEndingWith(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-name-contain")
    public ResponseEntity<List<User>> handleUsersNameContain(@RequestParam String name){

        List<User> users= service.getUsersByNameContaining(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-by-order")
    public ResponseEntity<List<User>> handleUsersByAgeOrder(@RequestParam String name){

        List<User> users= service.getUsersByAgeOrderName(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-by-last-order")
    public ResponseEntity<List<User>> handleUsersByAgeOrderName(@RequestParam String name){

        List<User> user = service.getUsersByLastNameOrder(name);
        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @PostMapping("/users-not")
    public ResponseEntity<List<User>> handleUsersByNotName(@RequestParam String name){

        List<User> users=service.getUsersByLastNameNot(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-in-age")
    public ResponseEntity<List<User>> handleUsersByAgeIn(@RequestBody Collection<Integer>ages){

        List<User> users=service.getUsersByAgeIn(ages);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-not-in")
    public ResponseEntity<List<User>> handleUsersByNotIn(@RequestBody Collection<String>name){

        List<User>users =service.getUsersByNameNotIn(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-married")
    public ResponseEntity<List<User>> handleUsersByMarried(){

        List<User>users =service.getUsersByMarried();
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

    @PostMapping("/users-not-married")
    public ResponseEntity<List<User>> handleGetUserByNotMarriedAndLastName(@RequestBody String name){

        List<User> users =service.getUserByNotMarriedAndLastName(name);
        return new ResponseEntity<>(users,HttpStatus.OK);
    }

//    Exercises related to Create,Delete,Modify,Read

    @GetMapping("/users/{id}")
    public ResponseEntity<User> handleGetUserByID(@PathVariable int  id){
        User user =service.getUserById(id);
        return new ResponseEntity<>(user,HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<HttpStatus> handleCreateUser(@RequestBody User user){
        service.CreateUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<HttpStatus> handleDeleteUser(@PathVariable int id){
        service.DeleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> handleUpdateUser(@PathVariable int id,@RequestBody User user){
       User user1= service.UpdateUser(id,user);
       return new ResponseEntity<>(user1,HttpStatus.OK);

    }

    @GetMapping("/users/name/{id}")
    public ResponseEntity<String> handleReturnNameById(@PathVariable int id){
        String name=service.ReturnUserNameById(id);
        return new ResponseEntity<>(name,HttpStatus.OK);
    }


    @GetMapping("/users-id-name-age")
        public ResponseEntity<List<Object[]>>handleReturnDetails(){
            return new ResponseEntity<>(daoTemp.getCustomerByQuery(),HttpStatus.OK);
        }

    @GetMapping("/user-native")
    public ResponseEntity<List<User>> handleGetUserByNativeQuery(@RequestParam String name){
        String conditionName="%"+name+"%";

        return new ResponseEntity<>(service.getUserByNativeQuery(conditionName),HttpStatus.OK);
    }

    }













