package com.infomovers.springpractice1.repository;

import com.infomovers.springpractice1.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    List<User> findByAddressAndDate(String address, LocalDate date);

    List<User> findByFirstnameOrLastname(String firstName,String lastName);

    List<User> findByFirstnameEquals(String firstName);

    List<User> findByDateBetween(LocalDate date1,LocalDate date2);

    List<User> findByIdBetween(int id1,int id2);

    List<User> findByAgeLessThan(int age);

    List<User> findByAgeLessThanEqual(int age);

    List<User> findByAgeGreaterThan(int age);

    List<User> findByAgeGreaterThanEqual(int age);

    List<User> findByDateAfter(LocalDate date);

    List<User> findByDateBefore(LocalDate date);

    List<User> findByIdAfter(int id);

    List<User> findByAgeIsNull();

    List<User> findByAgeNotNull();

    List<User> findByFirstnameIsNull();

    List<User> findByFirstnameLike(String name);

    List<User> findByFirstnameNotLike(String name);

    List<User> findByFirstnameStartingWith(String name);

    List<User> findByLastnameEndingWith(String name);

    List<User> findByFirstnameContaining(String name);

    List<User> findByFirstnameOrderByFirstname(String name);

    List<User> findByLastnameOrderByFirstnameDesc(String name);

    List<User> findByLastnameNot(String name);

    List<User> findByAgeIn(Collection<Integer>ages);

    List<User> findByFirstnameNotIn(Collection<String> name);

    List<User> findByMarriedTrue();

    List<User> findByMarriedFalseAndLastnameIs(String name);

    @Query(value="SELECT * FROM User u WHERE firstname LIKE ?1",nativeQuery = true)
    List<User> getUserByNameLikeNativeQuery(String name);



}
