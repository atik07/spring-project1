package com.infomovers.springpractice1.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "User")
public class User {

    @Id
    private int id;
    private String firstname;
    private String lastname;
    private String address;
    private LocalDate date;
    private int age;
    private boolean married;

    public User(){}

    public User(int id, String firstName, String lastNAme, String address, LocalDate date,  int age,boolean married) {

        this.id = id;
        this.firstname = firstName;
        this.lastname = lastNAme;
        this.address = address;
        this.date = date;
        this.age = age;
        this.married=married;

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstName) {
        this.firstname = firstName;
    }

    public String getLastNAme() {
        return lastname;
    }

    public void setLastNAme(String lastNAme) {
        lastname = lastNAme;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getMarried() {
        return married;
    }
    public void setMarried(boolean marriedStatus) {
        married = married;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address='" + address + '\'' +
                ", date=" + date +
                ", age=" + age +
                ", Married=" + married +
                '}';
    }
}

