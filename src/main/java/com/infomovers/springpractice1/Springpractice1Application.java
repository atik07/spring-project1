package com.infomovers.springpractice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Springpractice1Application {

    public static void main(String[] args) {
        SpringApplication.run(Springpractice1Application.class, args);
    }

}
